# Med

import io.smallrye.mutiny.Uni;
import org.jboss.logging.Logger;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class DocumentProcessor {

    @Inject
    CBService cbService;

    @Inject
    CMService cmService;

    @Inject
    EmailService emailService;

    private static final Logger LOGGER = Logger.getLogger(DocumentProcessor.class);
    private final AtomicBoolean isSentForUser = new AtomicBoolean(false);
    private final AtomicBoolean isSentForPanel = new AtomicBoolean(false);

    public Uni<Map<String, Object>> processDocument(DocumentMessage documentMessage) {
        LOGGER.info("the number of calling this service!");

        List<String> documentList = new ArrayList<>();
        String[] hospitalName = {""};
        String[] target = {""};
        String[] uploadedBy = {""};
        String[] createdTime = {""};
        String[] folder = {""};
        String[] spajNumber = {""};
        String[] policyNumber = {""};

        Uni<Void> uniProcess = Uni.createFrom().voidItem();

        for (String docId : documentMessage.getId()) {
            uniProcess = uniProcess.chain(() -> 
                cbService.getDocumentDetailAsync(docId)
                    .onItem().transformToUni(docObj -> {
                        LOGGER.infof("success get document detail.. continue to upload to cm");

                        JsonObject paramObj = new JsonObject();
                        paramObj.put("documentId", docId);

                        Uni<Void> uploadUni = Uni.createFrom().voidItem();
                        if (!docObj.containsKey("cmStatus") || !docObj.getString("cmStatus").equals("Success")) {
                            uploadUni = cmService.uploadToCMAsync(docObj)
                                .onItem().transformToUni(ignore -> {
                                    LOGGER.infof("success upload to cm.. continue to update the document status");
                                    paramObj.put("cmStatus", "Success");
                                    return cbService.updateDocumentCmStatusAsync(paramObj);
                                })
                                .replaceWithVoid();
                        }

                        return uploadUni
                            .onItem().transformToUni(ignore -> {
                                paramObj.put("status", DocumentStatusConstant.success);
                                paramObj.put("nbStatus", "Failed");
                                paramObj.put("phsStatus", "Failed");

                                MedicalCheckUpDocument medicalCheckUpDocument = Json.decodeValue(docObj.toString(), MedicalCheckUpDocument.class);

                                Uni<Void> triggerUni = Uni.createFrom().voidItem();
                                if ((docObj.getString("target").equals("PHS") && (!docObj.containsKey("phsStatus") || !docObj.getString("phsStatus").equals("Success")))
                                        || (docObj.getString("target").equals("NB") && (!docObj.containsKey("nbStatus") || !docObj.getString("nbStatus").equals("Success")))) {
                                    triggerUni = triggerSelectedServiceAsync(docObj.getString("target"), docObj, paramObj);
                                }

                                return triggerUni
                                    .onItem().transformToUni(ignore2 -> {
                                        JsonObject paramCm = new JsonObject()
                                                .put("folder", medicalCheckUpDocument.getFolder())
                                                .put("value", medicalCheckUpDocument.getMedicalCheckUpMetaData().getUniqueId());
                                        return cmService.getCmInfoAsync(paramCm)
                                                .onItem().transformToUni(cmInfo -> {
                                                    paramObj.put("cmId", cmInfo.getString("pId"));
                                                    return cbService.updateDocumentStatusAsync(paramObj);
                                                })
                                                .replaceWithVoid();
                                    });
                            })
                            .invoke(() -> {
                                documentList.add(docObj.getString("name"));
                                hospitalName[0] = docObj.getString("hospitalName");
                                target[0] = docObj.getString("target");
                                uploadedBy[0] = docObj.getString("uploadedBy");
                                createdTime[0] = docObj.getString("createdTime");
                                folder[0] = docObj.getString("folder").equalsIgnoreCase("POLDOC") ? "policy documents" : "loose mails";
                                spajNumber[0] = docObj.getJsonObject("medicalCheckUpMetaData").getString("SPAJ_Number");
                                policyNumber[0] = docObj.getJsonObject("medicalCheckUpMetaData").getString("Policy_Number");
                            });
                    })
            );
        }

        return uniProcess
            .onItem().transformToUni(ignore -> {
                LOGGER.infof("continue to send email");

                Uni<Void> sendEmailUni = Uni.createFrom().voidItem();
                if (!isSentForUser.get()) {
                    sendEmailUni = emailService.sendEmailAsync(documentList, hospitalName[0], target[0],
                            uploadedBy[0], createdTime[0], folder[0], spajNumber[0], policyNumber[0])
                        .invoke(() -> isSentForUser.set(true))
                        .eventually(() -> {
                            isSentForUser.set(false);
                            return Uni.createFrom().voidItem();
                        });
                }

                Uni<Void> sendEmailPanelUni = Uni.createFrom().voidItem();
                if (!isSentForPanel.get()) {
                    sendEmailPanelUni = emailService.sendEmailPanelAsync(documentList,
                            uploadedBy[0], createdTime[0], spajNumber[0], policyNumber[0])
                        .invoke(() -> isSentForPanel.set(true))
                        .eventually(() -> {
                            isSentForPanel.set(false);
                            return Uni.createFrom().voidItem();
                        });
                }

                return Uni.combine().all().unis(sendEmailUni, sendEmailPanelUni).discardItems();
            })
            .replaceWith(Map.of("status", "success", "data", documentMessage))
            .onFailure().invoke(e -> LOGGER.error("error", e));
    }

    private Uni<Void> triggerSelectedServiceAsync(String target, JsonObject docObj, JsonObject paramObj) {
        // Implement the async version of the triggerSelectedService method
        // This could involve calling async methods on your services
        // Example:
        // return someService.triggerAsync(target, docObj, paramObj);
        return Uni.createFrom().voidItem(); // Placeholder
    }
}




import io.smallrye.mutiny.Uni;
import org.jboss.logging.Logger;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class DocumentProcessor {

    @Inject
    CBService cbService;

    @Inject
    CMService cmService;

    @Inject
    EmailService emailService;

    private static final Logger LOGGER = Logger.getLogger(DocumentProcessor.class);
    private final AtomicBoolean isSentForUser = new AtomicBoolean(false);
    private final AtomicBoolean isSentForPanel = new AtomicBoolean(false);

    @Blocking
    public Uni<Map<String, Object>> processDocument(DocumentMessage documentMessage) {
        return Uni.createFrom().item(documentMessage)
                .onItem().transformToUni(this::processEachDocument)
                .onItem().transformToUni(docMessage -> sendEmails(docMessage, documentList))
                .onItem().transform(docMessage -> Map.of("status", "success", "data", docMessage))
                .onFailure().invoke(e -> LOGGER.error("error", e));
    }

    private Uni<DocumentMessage> processEachDocument(DocumentMessage documentMessage) {
        List<String> documentList = new ArrayList<>();
        String hospitalName = "";
        String target = "";
        String uploadedBy = "";
        String createdTime = "";
        String folder = "";
        String spajNumber = "";
        String policyNumber = "";

        Uni<Void> processing = Uni.createFrom().voidItem();

        for (String docId : documentMessage.getId()) {
            processing = processing.chain(() -> cbService.getDocumentDetailAsync(docId)
                    .onItem().transformToUni(docObj -> {
                        LOGGER.info("success get document detail.. continue to upload to cm");

                        JsonObject paramObj = new JsonObject().put("documentId", docId);
                        if (!docObj.containsKey("cmStatus") || !"Success".equals(docObj.getString("cmStatus"))) {
                            return cmService.uploadToCMAsync(docObj)
                                    .onItem().transformToUni(uploadResult -> {
                                        LOGGER.info("success upload to cm.. continue to update the document status");
                                        paramObj.put("cmStatus", "Success");
                                        return cbService.updateDocumentCmStatusAsync(paramObj);
                                    }).replaceWith(docObj);
                        }
                        return Uni.createFrom().item(docObj);
                    })
                    .onItem().transformToUni(docObj -> {
                        JsonObject paramObj = new JsonObject()
                                .put("documentId", docId)
                                .put("status", DocumentStatusConstant.success)
                                .put("nbStatus", "Failed")
                                .put("phsStatus", "Failed");

                        MedicalCheckUpDocument medicalCheckUpDocument = Json.decodeValue(docObj.toString(), MedicalCheckUpDocument.class);

                        if (("PHS".equals(docObj.getString("target")) && (!docObj.containsKey("phsStatus") || !"Success".equals(docObj.getString("phsStatus"))))
                                || ("NB".equals(docObj.getString("target")) && (!docObj.containsKey("nbStatus") || !"Success".equals(docObj.getString("nbStatus"))))) {
                            return triggerSelectedServiceAsync(docObj.getString("target"), docObj, paramObj);
                        }

                        return Uni.createFrom().voidItem();
                    })
                    .onItem().transformToUni(voidItem -> {
                        JsonObject paramCm = new JsonObject()
                                .put("folder", medicalCheckUpDocument.getFolder())
                                .put("value", medicalCheckUpDocument.getMedicalCheckUpMetaData().getUniqueId());
                        return cmService.getCmInfoAsync(paramCm)
                                .onItem().transformToUni(cmInfo -> {
                                    paramObj.put("cmId", cmInfo.getString("pId"));
                                    return cbService.updateDocumentStatusAsync(paramObj);
                                });
                    })
                    .invoke(() -> {
                        documentList.add(docObj.getString("name"));
                        hospitalName = docObj.getString("hospitalName");
                        target = docObj.getString("target");
                        uploadedBy = docObj.getString("uploadedBy");
                        createdTime = docObj.getString("createdTime");
                        folder = "POLDOC".equalsIgnoreCase(docObj.getString("folder")) ? "policy documents" : "loose mails";
                        spajNumber = docObj.getJsonObject("medicalCheckUpMetaData").getString("SPAJ_Number");
                        policyNumber = docObj.getJsonObject("medicalCheckUpMetaData").getString("Policy_Number");
                    }));
        }
        return processing.replaceWith(documentMessage);
    }

    private Uni<Void> sendEmails(DocumentMessage documentMessage, List<String> documentList) {
        return Uni.combine().all().unis(
                sendUserEmail(documentList),
                sendPanelEmail(documentList)
        ).discardItems();
    }

    private Uni<Void> sendUserEmail(List<String> documentList) {
        if (!isSentForUser.get()) {
            return emailService.sendEmailAsync(documentList, hospitalName, target,
                    uploadedBy, createdTime, folder, spajNumber, policyNumber)
                    .invoke(() -> isSentForUser.set(true))
                    .eventually(() -> isSentForUser.set(false));
        }
        return Uni.createFrom().voidItem();
    }

    private Uni<Void> sendPanelEmail(List<String> documentList) {
        if (!isSentForPanel.get()) {
            return emailService.sendEmailPanelAsync(documentList,
                    uploadedBy, createdTime, spajNumber, policyNumber)
                    .invoke(() -> isSentForPanel.set(true))
                    .eventually(() -> isSentForPanel.set(false));
        }
        return Uni.createFrom().voidItem();
    }

    private Uni<Void> triggerSelectedServiceAsync(String target, JsonObject docObj, JsonObject paramObj) {
        // Implement the async version of the triggerSelectedService method
        // This could involve calling async methods on your services
        // Example:
        // return someService.triggerAsync(target, docObj, paramObj);
        return Uni.createFrom().voidItem(); // Placeholder
    }
}


 CompletableFuture<Void> userEmailFuture = null;
            CompletableFuture<Void> panelEmailFuture = null;

            if (!isSentForUser.get()) {
                userEmailFuture = CompletableFuture.runAsync(() -> {
                    emailService.sendEmail(documentList, hospitalName, target, uploadedBy, createdTime, folder, spajNumber, policyNumber);
                    isSentForUser.set(true);
                }, executorService);
            }

            if (!isSentForPanel.get()) {
                panelEmailFuture = CompletableFuture.runAsync(() -> {
                    emailService.sendEmailPanel(documentList, uploadedBy, createdTime, spajNumber, policyNumber);
                    isSentForPanel.set(true);
                }, executorService);
            }

            // Ensure both futures complete
            CompletableFuture<Void> allFutures = CompletableFuture.allOf(userEmailFuture, panelEmailFuture);
            allFutures.join(); // Wait for all futures to complete

public Map<String, Object> processDocument(DocumentMessage documentMessage) throws Exception {
        try {
            LOGGER.info("the number of calling this service!");
            //variable untuk keperluan mengirim email
            List<String> documentList = new ArrayList<String>();
            String hospitalName = "";
            String target = "";
            String uploadedBy = "";
            String createdTime = "";
            String folder = "";
            String spajNumber = "";
            String policyNumber = "";

            //proses upload setiap document ke CM
            for (String docId : documentMessage.getId()) {
                //get detail document dari CB
                JsonObject docObj = cbService.getDocumentDetail(docId);
                Log.infof("success get document detail.. continue to upload to cm");

                JsonObject paramObj = new JsonObject();
                paramObj.put("documentId", docId);

                //upload document ke cm
                if (!docObj.containsKey("cmStatus") || !docObj.getString("cmStatus").equals("Success")) {
                    cmService.uploadToCM(docObj);
                    Log.infof("success upload to cm.. continue to update the document status");
                    paramObj.put("cmStatus", "Success");
                    cbService.updateDocumentCmStatus(paramObj);
                    paramObj.remove("cmStatus");
                }

                // call create log cm loosemails
                paramObj.put("status", DocumentStatusConstant.success);
                paramObj.put("nbStatus", "Failed");
                paramObj.put("phsStatus", "Failed");

                MedicalCheckUpDocument medicalCheckUpDocument = Json.decodeValue(docObj.toString(), MedicalCheckUpDocument.class);

                // hit selected service based on target document
                if ((docObj.getString("target").equals("PHS") && (!docObj.containsKey("phsStatus") || !docObj.getString("phsStatus").equals("Success")))
                        || (docObj.getString("target").equals("NB") && (!docObj.containsKey("nbStatus") || !docObj.getString("nbStatus").equals("Success")))) {
                    triggerSelectedService(docObj.getString("target"), docObj, paramObj);
                }

                // find cm id
                JsonObject paramCm = new JsonObject()
                        .put("folder", medicalCheckUpDocument.getFolder())
                        .put("value", medicalCheckUpDocument.getMedicalCheckUpMetaData().getUniqueId());
                JsonObject cmInfo = cmService.getCmInfo(paramCm);
                paramObj.put("cmId", cmInfo.getString("pId"));

                //update document status menjadi success
                cbService.updateDocumentStatus(paramObj);

                //isi variable email
                documentList.add(docObj.getString("name"));
                hospitalName = docObj.getString("hospitalName");
                target = docObj.getString("target");
                uploadedBy = docObj.getString("uploadedBy");
                createdTime = docObj.getString("createdTime");
                folder = docObj.getString("folder").equalsIgnoreCase("POLDOC") ? "policy documents" : "loose mails";
                spajNumber = docObj.getJsonObject("medicalCheckUpMetaData").getString("SPAJ_Number");
                policyNumber = docObj.getJsonObject("medicalCheckUpMetaData").getString("Policy_Number");
            }
            Log.infof("continue to send email");
            if (!isSentForUser.get()) {
                emailService.sendEmail(documentList, hospitalName, target,
                        uploadedBy, createdTime, folder, spajNumber, policyNumber);
                isSentForUser.set(true);
            }

            if (!isSentForPanel.get()) {
                emailService.sendEmailPanel(documentList,
                        uploadedBy, createdTime, spajNumber, policyNumber);
                isSentForPanel.set(true);
            }
            Log.infof("success sending the email!");
            isSentForUser.set(false);
            isSentForPanel.set(false);
            return Map.of("status", "success", "data", documentMessage);
        } catch (Exception e) {
            LOGGER.error("error", e);
            throw e;
        }
    }


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yusufprasatya/med.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yusufprasatya/med/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
